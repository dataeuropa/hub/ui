import vue from '@vitejs/plugin-vue';
import { lstatSync } from 'node:fs';
import path from 'path';
import { defineConfig } from 'vite';
import config from './config';

function isSymlink(pkg: string): boolean {
  const packagePath = path.resolve('..', '..', 'node_modules', pkg);
  try {
    return lstatSync(packagePath).isSymbolicLink();
  } catch {
    return false;
  }
}

// let buildMode;
// if (process.env.NODE_ENV === 'production') {
//   buildMode = process.env.BUILD_MODE === 'test' ? 'test' : 'build';
// } else {
//   buildMode = 'dev';
// }

const buildMode = process.env.NODE_ENV === 'production'
  ? process.env.BUILD_MODE === 'test' 
  ? 'test'
  : 'build'
  : 'dev';

const buildConfig = {
  BASE_PATH: (config[buildMode]).assetsPublicPath,
  // Commenting out because serviceUrl doesn't exist anymore
  // SERVICE_URL: config[buildMode].serviceUrl,
};

export default defineConfig({
  base: buildConfig.BASE_PATH,
  plugins: [
    vue(
      { template: { compilerOptions: { whitespace: 'preserve' } } }
    ),
  ],
  server: {
    port: 8080,
    // proxy requests to https://piveau-auth-middleware-evaluation.apps.osc.fokus.fraunhofer.de/ as /api due to CORS issues
    proxy: {
      '/api': {
        target:
          'https://piveau-auth-middleware-evaluation.apps.osc.fokus.fraunhofer.de/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    },
  },
  define: {
    // Shim process.env from webpack
    'process.env': {
      buildconf: buildConfig
    }
  },

  resolve: {
    alias: [
      // {
      //   find: '@piveau/piveau-hub-ui-modules/styles',
      //   replacement: path.resolve(__dirname, '../../piveau-ui/packages/piveau-hub-ui-modules/dist/piveau-hub-ui-modules.css')
      // },
      // {
      //   find: '@piveau/piveau-hub-ui-modules',
      //   replacement: path.resolve(__dirname, '../../piveau-ui/packages/piveau-hub-ui-modules/lib/index.ts')
      // },
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src')
      },
      {
        find: '@modules-scss',
        replacement: isSymlink('@piveau/piveau-hub-ui-modules') ?
          path.resolve(__dirname, '..', '..', 'node_modules', '@piveau/piveau-hub-ui-modules', 'dist', 'scss')
          : path.resolve(__dirname, 'node_modules', '@piveau/piveau-hub-ui-modules', 'dist', 'scss')
      },
      {
        find: /^~(.*)$/,
        replacement: '$1',
      },
      {
        find: 'lodash',
        replacement: 'lodash-es',
      },
      {
        find: 'vue-i18n',
        replacement: 'vue-i18n/dist/vue-i18n.cjs.js',
      },
    ],
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    preserveSymlinks: false
  },

  build: {
    rollupOptions: {
      output: {
        entryFileNames: 'app.[hash].js',
      }
    }
  }
});
