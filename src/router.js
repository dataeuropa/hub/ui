/* eslint-disable */
import * as Router from 'vue-router';
import { glueConfig as GLUE_CONFIG } from '../config/user-config';
import {
  Auth,
  DatasetDetailsDataset,
  MapBasic,
  MapBoundsReceiver,
  Imprint,
  PrivacyPolicy,
  EmbedDataset,
  EmbedDatasetSnippet,
} from "@piveau/piveau-hub-ui-modules";
import NotFound from "./pages/NotFound.vue"
import AdvancedSearch from './components/advanced-search/AdvancedSearch.vue';


import { LoginRedirect } from '@piveau/piveau-hub-ui-plugin-auth';
import Combined from "@/components/search/combined/Combined";

// Load components
const Datasets = () => import(/* webpackChunkName: "Datasets" */ /* webpackPrefetch: false */'@/components/search/datasets/Datasets');
const Catalogues = () => import(/* webpackChunkName: "Catalogues" */ /* webpackPrefetch: false */'@/components/search/catalogues/Catalogues');
const ECDatasetDetailsQuality = () => import(/* webpackChunkName: "ECDatasetDetailsQuality" */ /* webpackPrefetch: false */'@/components/datasetDetails/ECDatasetDetailsQuality');
const DatasetDetailsSimilarDatasets = () => import(/* webpackChunkName: "DatasetDetailsSimilarDatasets" */'@/components/datasetDetails/DatasetDetailsSimilarDatasets');
const DatasetDetailsCategories = () => import(/* webpackChunkName: "DatasetDetailsCategories" */'@/components/datasetDetails/DatasetDetailsCategories');


const DatasetDetails = () => import(/* webpackChunkName: "DatasetDetails" */ /* webpackPrefetch: false */'./components/DatasetDetails.vue');


// Load all DPI components in a dedicated chunk
const DataProviderInterface = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/DataProviderInterface');
const OverviewPage = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/OverviewPage');
const InputPage = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/InputPage');
const DraftsPage = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/DraftsPage');
const LinkedDataViewer = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/LinkedDataViewer');
const UserProfilePage = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/UserProfilePage');
const UserCataloguesPage = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/UserCataloguesPage');
const DataFetchingComponent = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/DataFetchingComponent');
const CatalogueMQA = () => import(/* webpackChunkName: "DataProviderInterface" */'@/components/data-provider-interface/CatalogueMQA');


const title = GLUE_CONFIG.metadata.title;

const rootBreadcrumb = [{
  text: 'Home',
  to: null,
  href: '/'
}];


const router = Router.createRouter({
  history: Router.createWebHistory(GLUE_CONFIG.routing.routerOptions.base),
  linkActiveClass: 'active',
  // mode: GLUE_CONFIG.routing.routerOptions.mode,
  scrollBehavior(to, from, savedPosition) {
    if (to.matched.some(route => route.meta.scrollTop)) return { left: 0, top: 0 };
    else if (savedPosition) return savedPosition;
    else return { left: 0, top: 0 };
  },
  routes: [
    {
      path: '/',
      redirect: { name: 'Datasets' },
    },
    {
      path: '/datasets/advanced',
      name: 'AdvancedSearch',
      component: AdvancedSearch,
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          return [
            ...rootBreadcrumb,
            {
              text: "Advanced Search",
              to: { name: 'AdvancedSearch' },
          }
          ]
        },
      },
    },
    {
      path: '/datasets',
      name: 'Datasets',
      component: Datasets,
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          const catalog = route.query?.showcatalogdetails && route.query?.catalog;
          const datasetOrCatalogBreadcrumbs = catalog
            ? [{
                text: t.call(vm, 'message.header.navigation.data.catalogs'),
                to: { name: 'Catalogues' },
              },
              {
                text: langResolverFn(store.getters['catalogDetails/getCatalog']?.title),
                to: { name: 'Datasets', query: { showcatalogdetails: true, catalog } },
              }
            ]
            : [{
                text: t.call(vm, 'message.header.navigation.data.datasets'),
                to: { name: 'Datasets' },
            }]
          return [
            ...rootBreadcrumb,
            ...datasetOrCatalogBreadcrumbs,
          ]
        },
      },
    },
    {
      path: '/datasets/:ds_id',
      component: DatasetDetails,
      children: [
        {
          path: '',
          name: 'DatasetDetailsDataset',
          components: {
            datasetDetailsSubpages: DatasetDetailsDataset,
          },
        },
        {
          path: 'categories',
          name: 'DatasetDetailsCategories',
          components: {
            datasetDetailsSubpages: DatasetDetailsCategories,
          },
          meta: {
            breadcrumbs({route, store, t, langResolverFn}) {
              const vm = this;
              return [{
                text: t.call(vm, 'message.metadata.categories'),
                to: { name: 'DatasetDetailsCategories', params: { ds_id: route.params.ds_id } },
              }]
            }

          },
        },
        {
          path: 'similarDatasets',
          name: 'DatasetDetailsSimilarDatasets',
          components: {
            datasetDetailsSubpages: DatasetDetailsSimilarDatasets,
          },
          meta: {
            breadcrumbs({route, store, t, langResolverFn}) {
              const vm = this;
              return [{
                text: t.call(vm, 'message.similarDatasets.similarDatasets'),
                to: { name: 'DatasetDetailsSimilarDatasets', params: { ds_id: route.params.ds_id } },
              }]
            }

          },
        },
        {
          path: 'quality',
          name: 'DatasetDetailsQuality',
          components: {
            datasetDetailsSubpages: ECDatasetDetailsQuality,
          },
          meta: {
            breadcrumbs({route, store, t, langResolverFn}) {
              const vm = this;
              return [{
                text: t.call(vm, 'message.datasetDetails.subnav.quality'),
                to: { name: 'DatasetDetailsQuality', params: { ds_id: route.params.ds_id } },
              }]
            },
          },
        },
        {
          path: 'embed',
          name: 'EmbedDataset',
          components: {
            datasetDetailsSubpages: EmbedDataset,
          },
          meta: {
            title,
          }
        },
        {
          path: 'embed-snippet',
          name: 'EmbedDatasetSnippet',
          components: {
            datasetDetailsSubpages: EmbedDatasetSnippet,
          },
          meta: {
            title,
          }
        },
      ],
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          const datasetTitle = store.getters['datasetDetails/getTitle'];

          return [
            ...rootBreadcrumb,
            {
              text: t.call(vm, 'message.header.navigation.data.datasets'),
              to: { name: 'Datasets' },
            },
            {
              text: langResolverFn(datasetTitle),
              to: { name: 'DatasetDetailsDataset' },
            }
          ]
        },
      },
    },
    {
      path: '/catalogues',
      name: 'Catalogues',
      component: Catalogues,
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          return [
            ...rootBreadcrumb,
            {
              text: t.call(vm, 'message.header.navigation.data.catalogs'),
              to: { name: 'Catalogues' },
            },
          ]
        },
      },
    },
    {
      path: '/catalogues/:ctlg_id',
      name: 'CatalogueDetails',
      component: Datasets,
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          return [
            ...rootBreadcrumb,
            ...[{
              text: t.call(vm, 'message.header.navigation.data.catalogs'),
              to: { name: 'Catalogues' },
              },
              {
                text: langResolverFn(store.getters['catalogDetails/getCatalog']?.title),
                to: { name: 'CatalogueDetails', params: {ctlg_id: route.params.ctlg_id }, query: { showcatalogdetails: true } },
            }],
          ]
        },
      },
    },
    {
      path: '/combined',
      name: 'Combined',
      component: Combined,
      meta: {
        breadcrumbs({route, store, t, langResolverFn}) {
          const vm = this;
          return [
            ...rootBreadcrumb,
            {
              text: "Combined",
              to: { name: 'Combined' },
            },
          ]
        },
      },
    },
    {
      path: '/imprint',
      name: 'Imprint',
      component: Imprint,
    },
    {
      path: '/privacypolicy',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy,
    },
    {
      path: '/maps',
      name: 'MapBasic',
      component: MapBasic,
    },
    {
      path: '/mapsBoundsReceiver',
      name: 'MapBoundsReceiver',
      component: MapBoundsReceiver,
    },
    {
      path: '/login',
      name: 'Login',
      component: Auth,
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Auth,
    },
    {
      path: '/login-redirect',
      name: 'LoginRedirect',
      component: LoginRedirect,
    },
    {
      path: '/logout-redirect',
      redirect: { name: 'Datasets' },
    },
    {
      path: '/404',
      alias: '/(.)*',
      name: 'NotFound',
      component: NotFound,
    },
    {
      path: '/sparql',
      name: 'SparqlSearch',
      component: () => import(/* webpackChunkName: "sparql" */ '@/components/SparqlSearch'),
    },
  ]
});

if (GLUE_CONFIG.content.dataProviderInterface.useService) {
  router.addRoute({
    path: '/dpi/draft',
    name: 'DataProviderInterface-Draft',
    component: DraftsPage,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "Datasets", to: { name: "Datasets" } },
        { text: "Draft Datasets", to: { name: "DataProviderInterface-Draft" } },
      ],
    },
  }),
  router.addRoute({
    path: '/dpi/draft/:name.:format',
    name: 'DataProviderInterface-LinkedData',
    component: LinkedDataViewer,
    props: true,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "Datasets", to: { name: "Datasets" } },
        { text: "Draft Datasets", to: { name: "DataProviderInterface-Draft" } },
        { text: "Linked Data", to: { name: "DataProviderInterface-LinkedData" } },
      ],
    },
  }),
  router.addRoute({
    path: '/dpi/user/',
    name: 'DataProviderInterface-UserProfile',
    component: UserProfilePage,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "Datasets", to: { name: "Datasets" } },
        { text: "User Profile", to: { name: "DataProviderInterface-UserProfile" } },
      ],
    },
  }),
  router.addRoute({
    path: '/dpi/user-catalogues',
    name: 'DataProviderInterface-UserCatalogues',
    component: UserCataloguesPage,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "Datasets", to: { name: "Datasets" } },
        { text: "My Catalogues", to: { name: "DataProviderInterface-UserCatalogues" } },
      ],
    },
  }),
  router.addRoute({
    path: '/dpi/user-catalogues/settings/:id',
    name: 'DataProviderInterface-MQASettings',
    component: CatalogueMQA,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "My Catalogues", to: { name: "DataProviderInterface-UserCatalogues" } },
        { text: "MQA Settings", to: { name: "DataProviderInterface-MQASettings" } },
      ],
    },
  }),
  router.addRoute({
    path: '/dpi/edit/:catalog/:property/:id',
    name: "DataProviderInterface-Edit",
    component: DataFetchingComponent,
    props: true
  }),
  router.addRoute({
    path: "/dpi",
    name: "DataProviderInterface",
    component: DataProviderInterface,
    meta: {
      requiresAuth: true,
      breadcrumbs: [
        ...rootBreadcrumb,
        { text: "Data Provider Interface", to: { name: "DataProviderInterface-Home" } },
      ],
    },
    children: [
      {
        path: ":property",
        name: "DataProviderInterface-Input",
        component: InputPage,
        props: true
      },
      // {
      //   path: ":property/overview",
      //   name: "DataProviderInterface-Overview",
      //   component: OverviewPage,
      //   props: true
      // },
      // {
      //   path: ":property/:page",
      //   name: "DataProviderInterface-Input",
      //   component: InputPage,
      //   props: true,
      //   meta: {
      //     breadcrumbs({route, store, t, langResolverFn}) {
      //       const editMode = store.getters['auth/getIsEditMode'];
      //       const draftMode = store.getters['auth/getIsDraft'];
      //       const property = route.params?.property;
      //       const catalog = store.getters['dpiStore/getData'](property)['dct:catalog'];
      //       const datasetID = store.getters['dpiStore/getData'](property)['@id'];
      //       const editBreadcrumb = editMode
      //         ? [
      //             (draftMode
      //               ? {
      //                   text: datasetID,
      //                   to: { name: "DataProviderInterface-Draft" },
      //                 }
      //               : {
      //                   text: datasetID,
      //                   to: { path: `/datasets/${datasetID}` },
      //                 }
      //             ),
      //             {
      //               text: 'Edit Dataset',
      //               to: { path: `/dpi/edit/${catalog}/${property}/${datasetID}` },
      //             },
      //           ]
      //         : [
      //             {
      //               text: "Create Dataset",
      //               to: { name: "DataProviderInterface" },
      //             },
      //           ];
      //       return [
      //         ...editBreadcrumb,
      //       ];
      //     },
      //   },
      //   children: [
      //     {
      //       path: ":id",
      //       name: "DataProviderInterface-ID",
      //       component: InputPage,
      //       props: true,
      //     },
      //   ],
      // },
    ]
  });
}

router.beforeEach((to, from, next) => {
  // Use a named group to match the file extension
  const fileExtension = '(?:\\.(rdf|n3|jsonld|ttl|nt))';

  // Use a named group to match the dataset ID
  const datasetId = `([a-z0-9-_]+)`;

  // Use the named groups to form the regular expression
  const regexString = `^\\/datasets\\/${datasetId}${fileExtension}`;

  // Create the regular expression object
  const datasetIdRegex = new RegExp(regexString);

  // Check if the URL matches the regular expression
  const match = to.path.match(datasetIdRegex);
  // if the URL matches the regular expression then console.log the dataset ID
  if (match) {
    const datasetId = match[1];
    const format = match[2];
    // redirect to the dataset details page with query dl={format}
    // DatasetDetails will then attempt to download the dataset using the dl query parameter
    next({ path: `/datasets/${datasetId}`, query: { dl: format } });
    return;
  }

  next();
});

export default router;
