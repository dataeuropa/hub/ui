import { KNOWN_DISALLOWED_CATALOG_IDS, KNOWN_ODP_CATALOG_IDS } from './constants';

export function getSubdomainCatalogIdFromUrl(url: string, disallowedCatalogIds : string[] = KNOWN_DISALLOWED_CATALOG_IDS): string {
  const host = new URL(url).host;
  const maybeSubdomain = getLeftmostSubdomain(host);
  return disallowedCatalogIds.includes(maybeSubdomain) ? '' : maybeSubdomain;
}

export function formatDatetime(date: string) {
  // return new Date(this.value).toLocaleDateString('de', { year: 'numeric', month: '2-digit', day: '2-digit' });
  // validate and format date to dd.mm.yyyy
  const dateObj = new Date(date);
  if (isNaN(dateObj.getTime())) {
    return '';
  }
  
  return dateObj.toLocaleDateString('de', { year: 'numeric', month: '2-digit', day: '2-digit' });
}

/**
 * Maps some locale codes to German region
 */
export const localeCodeToGermanMini = {
  // Just add a bunch of European countries.
  // We'll just need 'de' anyways, right?
  'de': 'Deutsch',
  'en': 'Englisch',
  'fr': 'Französisch',
  'es': 'Spanisch',
  'it': 'Italienisch',
  'pt': 'Portugiesisch',
  'nl': 'Niederländisch',
  'pl': 'Polnisch',
  'cs': 'Tschechisch',
  'da': 'Dänisch',
  'fi': 'Finnisch',
  'el': 'Griechisch',
  'hu': 'Ungarisch',
  'ga': 'Irisch',
  'lv': 'Lettisch',
  'lt': 'Litauisch',
  'mt': 'Maltesisch',
  'ro': 'Rumänisch',
  'sk': 'Slowakisch',
  'sl': 'Slowenisch',
  'sv': 'Schwedisch',
  'hr': 'Kroatisch',
  'bg': 'Bulgarisch',
  'et': 'Estnisch',
}

export function getLeftmostSubdomain(hostname: string) {
  // Strip port number if present
  const domainWithoutPort = hostname.split(':')[0];
  
  // Construct regex patterns
  const secondLevelPattern = '(co\\.uk|com\\.au|co\\.nz|co\\.za|com\\.br|staging\\.bydata\\.de|bydata\\.de)';
  // Update the generalPattern to match any subdomain before the main domain and TLD
  const generalPattern = new RegExp(`^(www\\.)?(.*?)\\.(?:[^\\.]+\\.${secondLevelPattern}|[^\\.]+\\.[^\\.]+)$`);
  const localPattern = /^([^.]+)\.local$/;
  
  // Match against .local pattern
  const localMatch = domainWithoutPort.match(localPattern);
  if (localMatch) {
      return localMatch[1];
  }
  
  // Match the subdomain for general domains
  const match = domainWithoutPort.match(generalPattern);
  
  if (match && match[2] && match[2] !== 'www') {
      // Split the subdomains and return the leftmost one
      return match[2].split('.')[0];
  }
  
  return '';
}

export async function isOdp(catalogId, hubSearchApiBaseUrl) {
  if (KNOWN_DISALLOWED_CATALOG_IDS.includes(catalogId)) {
    return false;
  }

  if (KNOWN_ODP_CATALOG_IDS.includes(catalogId)) {
    return true;
  }

  const url = `${hubSearchApiBaseUrl}catalogues/${catalogId}`;
  const response = await fetch(url, {method: 'GET'});
  return response.ok;
}
