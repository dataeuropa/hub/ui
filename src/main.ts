// @ts-nocheck
import $ from "jquery";
import { createI18n } from "vue-i18n";
import { createApp } from "vue";
import { createHead } from "@unhead/vue";
import VueProgressBar from "@aacassandra/vue3-progressbar";
import VueCookies from "vue3-cookies";
import VueClickAway from "vue3-click-away";
import { plugin as FormKitPlugin, defaultConfig } from "@formkit/vue";
import "@formkit/themes/genesis";
import config from "../formkit.config.ts";
import { Skeletor } from "vue-skeletor";
import "vue-skeletor/dist/vue-skeletor.css";


// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Meta from 'vue-meta';
import deuFooterHeader, { Header, Footer } from "@deu/deu-header-footer";
import UniversalPiwik from "./plugins/piveau-universal-piwik";

// Import Font Awesome Icons Library for vue
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faGoogle,
  faGooglePlus,
  faGooglePlusG,
  faFacebook,
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons";
import {
  faComment,
  faExternalLinkAlt,
  faPlus,
  faMinus,
  faArrowDown,
  faArrowUp,
  faInfoCircle,
  faExclamationTriangle,
  faThumbsUp as fasThummbsUp,
  faThumbsDown as fasThumbsDown,
} from '@fortawesome/free-solid-svg-icons';
import {
  faThumbsUp as farThumbsUp,
  faThumbsDown as farThumbsDown,
} from '@fortawesome/free-regular-svg-icons';
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the Data Grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the Data Grid
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { glueConfig as GLUE_CONFIG, i18n as I18N_CONFIG } from '../config/user-config.js';
import runtimeConfig from '../config/runtime-config.js';
import router from './router.js';

import App from "./App";
import {
  dateFilters,
  AppSnackbar,
  AppConfirmationDialog,
  bulkDownloadCorsProxyService,
  corsProxyService,
  runtimeConfigurationService,
  store,
  configureModules,
  InfoSlot,
  ConditionalInput,
  AutocompleteInput,
  UniqueIdentifierInput,
  FileUpload,
  helpers
} from '@piveau/piveau-hub-ui-modules';

import searchStore from './store/search';
store.registerModule('search', searchStore);

import '@piveau/piveau-hub-ui-modules/styles';

import { init } from "@piveau/piveau-hub-ui-plugin-auth";

import ECSelectFacet from "@/components/ECSelectFacet";
import ECRadioFacet from "@/components/ECRadioFacet";
import ECMore from "@/components/ECMore";
import ECButton from "@/components/ECButton";
import ECInfo from "./components/ECInfo.vue";
import ECLinkButton from "./components/ECLinkButton.vue";
import ECDataInfoBox from "./components/ECDataInfoBox.vue";
import ECDistributionDetails from "@/components/datasetDetails/ECDistributionDetails";
import ECDistributionsHeader from "@/components/datasetDetails/ECDistributionsHeader";
import ECSubNavigation from "@/components/ECSubNavigation.vue";
import ECDistributionVisualisation from "@/components/distribtutionVisualisation/DistributionVisualisation.vue";
import ECDatasetDetailsDataset from "@/components/datasetDetails/ECDatasetDetailsDataset.vue";
import DownloadAllDistributions from "@/components/DownloadAllDistributions.vue";

const app = createApp(App);

app.use(runtimeConfigurationService, runtimeConfig, {
  baseConfig: GLUE_CONFIG,
  debug: false,
});

const env = app.config.globalProperties.$env;

app.config.globalProperties.devtools = true;

const { plugin: pvAuthPlugin, navigationGuardFn } = init({
  baseUrl: env.authentication.authMiddleware.baseUrl,
  enabled:
    env?.authentication?.authMiddleware?.enable === "false"
      ? false
      : env?.authentication?.authMiddleware?.enable,

  refreshInterval: 120000,
  defaultLoginOptions: {
    redirect_url: `${window.location.origin}/data/login-redirect`,
    requestParameters: {
      acr_values:
        env.authentication.authMiddleware?.acrValues ||
        "https://ecas.ec.europa.eu/loa/medium",
    },
  },
  defaultLogoutOptions: {
    post_logout_redirect_url: `${window.location.origin}/data/logout-redirect`,
    redirect_url: `${window.location.origin}/data/logout-redirect`,
  },
});

app.use(pvAuthPlugin);

app.use(VueClickAway);

app.use(FormKitPlugin, defaultConfig(config));

const head = createHead();
app.use(head);

configureModules(app, store, {
  components: {
    DownloadAllDistributions,
    SelectFacet: ECSelectFacet,
    RadioFacet: ECRadioFacet,
    PvShowMore: ECMore,
    PvButton: ECButton,
    PvBanner: ECInfo,
    PvDataInfoBox: ECDataInfoBox,
    DatasetDetailsNavigationPage: ECLinkButton,
    DistributionDetails: ECDistributionDetails,
    DistributionsHeader: ECDistributionsHeader,
    SubNavigation: ECSubNavigation,
    DistributionVisualisationSlot: ECDistributionVisualisation,
    DatasetDetailsDataset: ECDatasetDetailsDataset,
  },
  services: GLUE_CONFIG.services,
  serviceParams: {
    baseUrl: env.api.baseUrl,
    qualityBaseUrl: env.api.qualityBaseUrl,
    similarityBaseUrl: env.api.similarityBaseUrl,
    similarityServiceName: env.api.similarityServiceName,
    gazetteerBaseUrl: env.api.gazetteerBaseUrl,
    hubUrl: env.api.hubUrl,
    keycloak: env.authentication.keycloak,
    rtp: env.authentication.rtp,
    useAuthService: env.authentication.useService,
    authToken: env.authentication.authToken,
    defaultScoringFacets:
      env.content.datasets.facets.scoringFacets.defaultScoringFacets,
  },
});

app.component("InfoSlot", InfoSlot);
app.component("ConditionalInput", ConditionalInput);
app.component("AutocompleteInput", AutocompleteInput);
app.component("UniqueIdentifierInput", UniqueIdentifierInput);
app.component("FileUpload", FileUpload);

app.component("AppSnackbar", AppSnackbar);
app.component("AppConfirmationDialog", AppConfirmationDialog);

app.component(Skeletor.name, Skeletor);

app.use(VueCookies);

app.use(corsProxyService, env.api.corsproxyApiUrl);
app.use(bulkDownloadCorsProxyService, GLUE_CONFIG, env.api.corsproxyApiUrl);

const { isPiwikPro, siteId, trackerUrl } = env.tracker;
app.use(UniversalPiwik, {
  // disabled: typeof env.tracker.disabled === "boolean" && env.tracker.disabled,
  router,
  isPiwikPro,
  trackerUrl,
  siteId,
  debug: process.env.NODE_ENV === "development",
  useSuspendFeature: false,
  pageViewOptions: {
    // Set this to true as long as navigating to the /datasets/ route
    // adds a 'minScore' query to prevent duplicated tracking
    useDatasetsMinScoreFix: false,
    // Send empty dataset metadata for every page view
    // See https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2098
    beforeTrackPageView: (to, from, tracker) => {
      if (to.name !== "DatasetDetailsDataset") {
        tracker.trackDatasetDetailsPageView(null, null, {
          dataset_AccessRights: "",
          dataset_AccrualPeriodicity: "",
          dataset_Catalog: "",
          dataset_ID: "",
          dataset_Publisher: "",
          dataset_Title: "",
        });
      }
    },
  },
});

// Vue i18n
const LOCALE = env.languages.locale;
const FALLBACKLOCALE = env.languages.fallbackLocale;

const i18n = createI18n({
  locale: LOCALE,
  fallbackLocale: FALLBACKLOCALE,
  messages: I18N_CONFIG,
  allowComposition: true,
  legacy: false,
  globalInjection: true,
  fallbackWarn: false,
  silentFallbackWarn: true,
  silentTranslationWarn: true,
  warnHtmlMessage: false,
});

app.config.globalProperties.i18n = i18n;
app.use(i18n);

// Set locale for dateFilters
dateFilters.setLocale(LOCALE);

// Bootstrap requirements to use js-features of bs-components
import "popper.js";

import "bootstrap";

import "./styles/styles.scss";
import "./styles/ec-style.scss";

$(() => {
  $('[data-toggle="popover"]').popover({ container: "body" });
  $('[data-toggle="tooltip"]').tooltip({ container: "body" });
});

import "@fortawesome/fontawesome-free/css/all.css";

import "@deu/deu-header-footer/dist/deu-header-footer.css";

// OpenStreetMaps popup styles
import "leaflet/dist/leaflet.css";

// Vue-progressbar setup
const progressBarOptions = {
  thickness: "5px",
  autoRevert: false,
  transition: {
    speed: "1.0s",
    opacity: "0.5s",
    termination: 1000,
  },
};
app.use(VueProgressBar, progressBarOptions);
import CodeDiff from "v-code-diff";

app.use(CodeDiff);
app.use(deuFooterHeader, { i18n });
app.component("deu-header", Header);
app.component("deu-footer", Footer);


// Add Font Awesome Icons
library.add(faGoogle, faGooglePlus, faGooglePlusG, faFacebook, faFacebookF, faInstagram, faTwitter, faLinkedinIn, faComment, faExternalLinkAlt, faPlus, faMinus, faArrowDown, faArrowUp, faInfoCircle, faExclamationTriangle, farThumbsUp, farThumbsDown, fasThummbsUp, fasThumbsDown);
app.component('font-awesome-icon', FontAwesomeIcon);

app.config.globalProperties.productionTip = false;

// Vue Router
app.use(router);
router.beforeEach(helpers.createStickyLocale(LOCALE || FALLBACKLOCALE));
router.beforeEach(navigationGuardFn);
router.app = app;

// Vuex Store
app.use(store);

app.mount("#app");
