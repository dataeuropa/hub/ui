/**
 * Vue plugin for streamlining common analytics tasks for both
 * Matomo and PiwikPro
 */
import PiwikProTracker from './trackers/piwikProTracker';

/**
 * Relevant for PiwikPro only.
 * Prevents from the initial route from being tracked.
 *
 * @see https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/1602
 */
let preventFirstRouteTrack = true;

let beforeTrackPageViewFn = () => {};

/**
 * Tracks page view on route changes
 * @param {*} router Vue router instance
 * @param {MatomoTracker | PiwikProTracker} tracker Piwik tracker
 */
export function trackRouteChanges(router, tracker, options) {
  const opts = {
    onlyTrackWithLocale: true,
    useDatasetsMinScoreFix: false,
    delay: 1000,
    trackPagePredicate: true,
    documentTitleResolver: () => "",
    ...options,
  };

  router.afterEach((to, from) => {
    if (tracker.isStopped) return;
    if (to.meta.ignoreAnalytics) return;

    const isSamePath = to.fullPath === (from && from.fullPath);
    const isSameRouteName = to.name === from.name;
    if (isSamePath && isSameRouteName) return;
    // Workaround: remove duplicated page views for Datasets page
    // because some default queries being added.
    // Possible fixes are to either implement dynamic filtering option
    // or remove that default query mechanism from router
    // (and add fallback values to store instead)
    const { query, name } = to;
    const isDatasetsPage = name === 'Datasets';
    const hasMinScoring = to.query && ('minScoring' in query);
    const hasLocale = to.query && ('locale' in to.query);
    if (opts.onlyTrackWithLocale && !hasLocale) return;
    if (opts.useDatasetsMinScoreFix && isDatasetsPage && !hasMinScoring) return;

    if (opts.debug && opts.verbose) {
      console.log('Track page view', from.fullPath, to.fullPath);
    }

    const url = router.resolve(to.fullPath).href;

    if (beforeTrackPageViewFn && typeof beforeTrackPageViewFn === 'function') {
      if (opts.verbose) console.log('Before track page view');
      beforeTrackPageViewFn(to, from, tracker);
    } else if (typeof beforeTrackPageViewFn !== 'function') {
      console.warn('beforeTrackPageViewFn must be a function');
    }

    const trackPagePredicateEval = typeof opts.trackPagePredicate === 'function'
      ? opts.trackPagePredicate(to, from)
      : opts.trackPagePredicate;
    if (!trackPagePredicateEval) {
      if (opts.verbose) console.log('I will not track this page view due to trackPagePredicate');
      return;
    }

    if (tracker.type === 'piwikpro') {
      if (!preventFirstRouteTrack) {
        // Workaround: Wait for a set amount of time before tracking the page
        // in order to send correct page title.
        // NOTICE: will send incorrect page title when visitor navigates to
        // another page before the timeout is triggered.
        setTimeout(() => {
          const baseTitle = document.title || to.meta.title || '';
          const resolvedDocumentTitle = typeof opts.documentTitleResolver === 'function'
            ? opts.documentTitleResolver(to, from, baseTitle)
            : baseTitle;
          tracker.trackPageView(url, resolvedDocumentTitle, {
            eventType: 'screen_load',
          });
        }, opts.delay);
      } else {
        // Workaround: send initial 'trackPageView' in the same delayed fashion
        // instead of letting the init script doing it for you and possibly
        // sending undefined title page
        preventFirstRouteTrack = false;
        if (opts.debug) console.log('Prevent first route track');
        setTimeout(() => {
          // eslint-disable-next-line no-underscore-dangle
          window._paq = window._paq || [];
          // eslint-disable-next-line no-underscore-dangle
          window._paq.push(['trackPageView']);
        }, opts.delay);
      }
    } else if (tracker.type === 'matomo') {
      // tracker.trackPageView(url);
      setTimeout(() => {
        const baseTitle = document.title || to.meta.title || '';
        const resolvedDocumentTitle = typeof opts.documentTitleResolver === 'function'
          ? opts.documentTitleResolver(to, from, baseTitle)
          : baseTitle;
        tracker.trackPageView(url, resolvedDocumentTitle);
      }, opts.delay);
    }
  });
}

const UniversalPiwik = {
  install(app, options) {
    const opts = {
      trackerUrl: undefined,
      siteId: undefined,
      router: undefined,
      isPiwikPro: false,
      debug: false,
      immediate: false,
      removeCookiesWhenNoConsent: true,
      stopWhenNoConsent: true,
      useSuspendFeature: false,
      verbose: false,
      disabled: false,
      requireConsent: 'consent',
      pageViewOptions: {
        useDatasetsMinScoreFix: false,
        onlyTrackWithLocale: true,
        delay: 1000,
        beforeTrackPageView: () => {},
        trackPagePredicate: true,
        documentTitleResolver: () => "",
      },
      ...options,
    };

    if (opts.debug && opts.verbose) {
      console.log('options = ', opts);
    }

    if (!opts.trackerUrl || !opts.siteId) {
      // eslint-disable-next-line no-console
      console.error('Error: trackerUrl or siteId is not specified');
    }

    // If no trackerUrl, use MatomoTracker as fallback so that tracking goes into limbo
    // inside _paq
    const trackerParams = [
      opts.trackerUrl,
      opts.siteId,
      {
        debug: opts.debug,
        immediate: opts.immediate,
        removeCookiesWhenNoConsent: opts.removeCookiesWhenNoConsent,
        stopWhenNoConsent: opts.stopWhenNoConsent,
        disabled: opts.disabled,
        useSuspendFeature: opts.useSuspendFeature,
        requireConsent: opts.requireConsent,
      },
    ];

    const tracker = opts.trackerUrl && new PiwikProTracker(...trackerParams)

    // eslint-disable-next-line no-underscore-dangle
    if (opts.immediate) tracker._init();

    if (opts.router) {

      app.use(opts.router);

      if (typeof opts.pageViewOptions.beforeTrackPageView === 'function') {
        beforeTrackPageViewFn = opts.pageViewOptions.beforeTrackPageView;
      }

      trackRouteChanges(opts.router, tracker, {
        debug: opts.debug, verbose: opts.verbose, ...opts.pageViewOptions,
      });
    }

    app.config.globalProperties.$piwik = tracker;
    app.config.globalProperties.$universalPiwik = {
      beforeTrackPageView: (cb) => {
        beforeTrackPageViewFn = cb;
      },
    };
  },
};

export default UniversalPiwik;
