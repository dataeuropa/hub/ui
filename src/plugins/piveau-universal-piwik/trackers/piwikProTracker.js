/* eslint-disable class-methods-use-this */
/* eslint-disable no-underscore-dangle */
import VuePiwikPro from '@piwikpro/vue-piwik-pro'
import { DataLayer } from '@piwikpro/vue-piwik-pro'

import clearPiwik from './utils';

export default class PiwikProTracker {
  constructor(trackerUrl, siteId, options) {
    window._paq = window._paq || [];
    this.type = 'matomo';
    this.trackerUrl = trackerUrl.slice(-1) === '/' ? trackerUrl.slice(0, -1) : trackerUrl;
    this.siteId = siteId;
    this.stopped = false;
    this.options = {
      debug: false,
      disabled: false,
      requireConsent: 'consent',
      ...options,
    };

    if (this.options.disabled) {
      return new Proxy(this, {
        get(target, prop) {
          const origMethod = target[prop];
          if (typeof origMethod === 'function') {
            return () => {};
          }
          return undefined;
        },
      });
    }


    this.type = 'piwikpro';
    this._interceptCounter = 0;
    this.suspended = this.options.suspended || false;
    this.useSuspendFeature = this.options.useUseSuspendFeature || false;
    window.dataLayer = window.dataLayer || [];

    if (this.useSuspendFeature) {
      this._activateSuspendFeature();
    }
    const old = window._paq.push;

    // Intercept the following commands
    // Only intercept their first occurence
    const interceptCommands = ['enableLinkTracking', 'trackPageView'];
    const interceptor = (args) => {
      if (Array.isArray(args) && args.length > 0) {
        const command = args[0];
        if (interceptCommands.includes(command)) {
          if (this.options.debug) console.log('intercept', command);
          interceptCommands.splice(interceptCommands.indexOf(command), 1);
          if (interceptCommands.length === 0) {
            if (this.options.debug) console.log('restore original _paq');
            window._paq.push = old;
          }
          return;
        }
      }
      old.apply(this, [args]);
    };
    window._paq.push = interceptor;
  }

  _activateSuspendFeature() {
    this.tmpDataLayer = [];
    this.suspendFilterFn = () => false;
    const that = this;
    window.dataLayer = new Proxy([], {
      get: (target, prop, receiver) => {
        if (prop === 'push') {
          return new Proxy(target.push, {
            apply(_target, thisArg, argumentsList) {
              return that.suspended && !that.suspendFilterFn(argumentsList)
                ? that.tmpDataLayer.push(argumentsList[0])
                : _target.apply(thisArg, argumentsList);
            },
          });
        }
        return Reflect.get(target, prop, receiver);
      },
    });
  }

  suspendFilter(filterFn) {
    if (!this.useSuspendFeature) return;

    this.suspended = true;
    this.suspendFilterFn = typeof filterFn === 'function' ? filterFn : () => false;
  }

  resume() {
    if (!this.useSuspendFeature) return;
    this.suspended = false;
    this.tmpDataLayer.forEach((item) => {
      window.dataLayer.push(item);
    });

    this.tmpDataLayer = [];
  }

  /* eslint-disable */
  _init() {
    if (this.options.debug) if (this.options.debug) console.log('init', this.type);
    if (this.options.debug) _paq.push(['setUserId', 'dev__debug2']);
    VuePiwikPro.initialize(
      this.siteId,
      this.trackerUrl,
    )
  }
  /* eslint-enable */

  init() {
    if (!this.options.immediate) {
      // eslint-disable-next-line no-underscore-dangle
      this._init();
    }
  }

  /* eslint-disable */
  consentGiven() {
    const new_consent = { consents: { } };
    new_consent.consents = { analytics: { status: 1 } };
    window.ppms && window.ppms.cm.api('setComplianceSettings', new_consent, (new_consent) => { console.log(new_consent); });
  }
  /* eslint-enable */

  /* eslint-disable */
  consentDeclined() {
    const new_consent = { consents: { } };
    new_consent.consents = { analytics: { status: 0 } };
    window.ppms && window.ppms.cm.api('setComplianceSettings', new_consent, (new_consent) => {
      // Optionally do additional tasks (like gdpr compliancy) when user has given no consent
      if (this.options.removeCookiesWhenNoConsent) setTimeout(clearPiwik, 1000);
      if (this.options.stopWhenNoConsent) this.stop();
      console.log(new_consent);
    });
  }
  /* eslint-enable */

  /* eslint-disable */
  consentNoDecision() {
    const new_consent = { consents: { } };
    new_consent.consents = { analytics: { status: -1 } };
    window.ppms && window.ppms.cm.api('setComplianceSettings', new_consent, (new_consent) => { console.log(new_consent); });
  }
  /* eslint-enable */

  /* eslint-disable class-methods-use-this */
  /* eslint-disable no-underscore-dangle */

  trackInteraction(eventType = 'screen_load', variables = {}) {
    DataLayer.push({
      event: 'analytics_interaction',
      event_type: eventType,
      ...variables,
    })
  }

  trackPageView(url, title, { eventType = 'screen_load', metadata = {} }) {
    this.trackInteraction(eventType, {
      ...title ? { screen_title: title } : {},
      ...url ? { page_url: url } : {},
      ...metadata,
    });
  }

  trackDatasetDetailsPageView(url, title, dataset) {
    this.trackPageView(url, title, {
      eventType: 'send_dataset_metadata',
      metadata: dataset,
    });
  }

  trackDownload(url, dimensions) {
    this.trackInteraction('download', {
      page_url: url,
      ...dimensions,
    });
    // super.trackDownload(url, dimensions);
  }

  trackOutlink(url, dimensions) {
    this.trackInteraction('outlink', {
      page_url: url,
      ...dimensions,
    });
    // super.trackOutlink(url, dimensions);
  }

  trackGotoResource() {
    this.trackInteraction('go_to_resource');
  }

  stop() {
    if (this.options.debug) console.log('Disabling window.dataLayer');
    window.dataLayer = {
      push() {},
    };
    this.stopped = true;
  }

  get isSuspended() {
    return this.suspended;
  }
}
