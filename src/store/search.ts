type SearchState = {
  editorialContentItems: [],
  editorialContentCount: Number | undefined,
  totalCount: Number | undefined
};

const state: SearchState = {
  editorialContentItems: [],
  editorialContentCount: undefined,
  totalCount: undefined
};

const GETTERS = {
  getEditorialContentItems: (state: SearchState) => state.editorialContentItems,
  getEditorialContentCount: (state: SearchState) => state.editorialContentCount,
  getTotalCount: (state: SearchState) => state.totalCount,
};

const actions = {
  setEditorialContentItems({ commit }, datasets) {
    commit('SET_EDITORIAL_CONTENT_ITEMS', datasets);
  },
  setEditorialContentCount({ commit }, count) {
    commit('SET_EDITORIAL_CONTENT_COUNT', count);
  },
  setTotalCount({ commit }, count) {
    commit('SET_TOTAL_COUNT', count);
  }
};

const mutations = {
  SET_EDITORIAL_CONTENT_ITEMS(state: SearchState, data) {
    state.editorialContentItems = data;
  },
  SET_EDITORIAL_CONTENT_COUNT(state: SearchState, count) {
    state.editorialContentCount = count;
  },
  SET_TOTAL_COUNT(state: SearchState, count) {
    state.totalCount = count;
  }
};

const module = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters: GETTERS,
};

export default module;
