const { defineConfig } = require("cypress");
const vitePreprocessor = require("cypress-vite");
const path = require("path");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      on(
        "file:preprocessor",
        vitePreprocessor({
          configFile: path.resolve(__dirname, "cypress", "./vite.config.ts"),
          mode: "development",
        })
      );
    },
    baseUrl: "http://localhost:4173",
    // defaultCommandTimeout: 12000
  },

  component: {
    specPattern: "src/**/*.cy.{js,jsx,ts,tsx}",
    setupNodeEvents(on, config) {
      on(
        "file:preprocessor",
        vitePreprocessor({
          configFile: path.resolve(__dirname, "cypress", "./vite.config.ts"),
          mode: "development",
        })
      );
    },
    devServer: {
      framework: 'vue',
      bundler: 'vite'
    }
  },
});
