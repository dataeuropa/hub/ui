var { merge } = require('webpack-merge');
import prodEnv from './prod.env';

export default merge(prodEnv, {
  NODE_ENV: '"development"'
});
