// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

import { mount } from 'cypress/vue';
import { glueConfig as GLUE_CONFIG, i18n as I18N_CONFIG } from '../../config/user-config';
import runtimeConfig from '../../config/runtime-config';
import { runtimeConfigurationService } from '@piveau/piveau-hub-ui-modules';
import router from '../../src/router';
import { createI18n } from 'vue-i18n';
import { createApp } from 'vue'
import * as Router from 'vue-router';
import UniversalPiwik from '@piveau/piveau-universal-piwik';

Cypress.Commands.add('mount', (component, options = {}) => {

  const app = createApp(App);

  options.global = options.global || {}
  options.global.plugins = options.global.plugins || []

  app.use(runtimeConfigurationService, runtimeConfig, { baseConfig: GLUE_CONFIG, debug: false });

  const env = Vue.prototype.$env;

  app.use(VueRouter);
  options.router = router;

  const LOCALE = env.languages.locale;
  const FALLBACKLOCALE = env.languages.fallbackLocale;

  const i18n = createI18n({
    locale: LOCALE,
    fallbackLocale: FALLBACKLOCALE,
    messages: I18N_CONFIG,
    allowComposition: true,
    legacy: true,
    globalInjection: true,
    fallbackWarn: false,
    silentFallbackWarn: true,
    silentTranslationWarn: true,
    warnHtmlMessage: false,
  });
  
  app.config.globalProperties.i18n = i18n;
  options.i18n = i18n;
  app.use(i18n);


  const { isPiwikPro, siteId, trackerUrl } = env.tracker;
  app.use(UniversalPiwik, {
    router,
    isPiwikPro,
    trackerUrl,
    siteId,
    debug: process.env.NODE_ENV === 'development',
    useSuspendFeature: false,
    pageViewOptions: {
      // Set this to true as long as navigating to the /datasets/ route
      // adds a 'minScore' query to prevent duplicated tracking
      useDatasetsMinScoreFix: false,
      // Send empty dataset metadata for every page view
      // See https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2098
      beforeTrackPageView: (to, from, tracker) => {
        if (to.name !== 'DatasetDetailsDataset') {
          tracker.trackDatasetDetailsPageView(null, null, {
            dataset_AccessRights: '',
            dataset_AccrualPeriodicity: '',
            dataset_Catalog: '',
            dataset_ID: '',
            dataset_Publisher: '',
            dataset_Title: '',
          });
        }
      },
    },
  });

  return mount(component, options);
});